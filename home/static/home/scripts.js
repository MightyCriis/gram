$(document).ready(function(){
    $('.likepost').click(function(){
        var postID = this.dataset.post;
        $.ajax({
            url: '/like_unlike/',
            method: 'POST',
            data: JSON.stringify({'postID':postID}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            success: function(data){
                var likebtn = $(".likepost[data-post = "+  postID +"]");
                var btncolor = likebtn.css('color');
                var numberOfLikes = data.likesNumber 
                if(btncolor === 'rgb(0, 0, 0)'){
                    likebtn.removeClass('fa fa-heart-o').addClass('fa fa-heart').css({'color':'red', 'fontSize':'24'});
                }
                else {
                    likebtn.removeClass('fa fa-heart').addClass('fa fa-heart-o').css({'color':'black'});
                }
                if (numberOfLikes == 1){
                    $(".likesCount[data-post = "+ postID+ "]").html(numberOfLikes + ' like');
                }
                else {
                    $(".likesCount[data-post = "+ postID+ "]").html(numberOfLikes + ' likes');
                }
            }
        })
    })
})

$(document).ready(function(){
    $('.unlike').click(function(){
        var postID = this.dataset.post;
        $.ajax({
            url: '/like_unlike/',
            method: 'POST',
            data: JSON.stringify({'postID':postID}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            success: function(data){
                var unlikebtn = $(".unlike[data-post = "+  postID +"]");
                var btncolor = unlikebtn.css('color');
                var numberOfLikes = data.likesNumber 
                if(btncolor == 'rgb(255, 0, 0)'){
                    unlikebtn.removeClass('fa fa-heart').addClass('fa fa-heart-o').css({'color':'black'});
                }
                else {
                    unlikebtn.removeClass('fa fa-heart-o').addClass('fa fa-heart').css({'color':'red', 'fontSize':'24'});
                }
                if (numberOfLikes == 1){
                    $(".likesCount[data-post = "+ postID+ "]").html(numberOfLikes + ' like');
                }
                else {
                    $(".likesCount[data-post = "+ postID+ "]").html(numberOfLikes + ' likes');
                }
            }
        })
    })
})

$(document).ready(function(){
    $('.addcomment').click(function(){
        var postID = this.dataset.post;
        var comment = $("input[data-post="+ postID +"]").val();
        $.ajax({
            url: '/comment/',
            method: 'POST',
            data: JSON.stringify({'postID':postID, 'comment':comment}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            success: function(){
                $("input[data-post="+ postID +"]").val('');
            }
        })
    })
})