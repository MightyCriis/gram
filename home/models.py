from django.db import models 
from django.contrib.auth.models import User 

# Create your models here.

class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, related_name='like', blank=True)
    caption = models.TextField()
    photo = models.ImageField(upload_to='posts')
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.caption
    
    def number_of_likes(self):
        return self.likes.count()

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return self.text