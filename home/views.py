from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, reverse
from django.contrib.auth.models import User  
from django.contrib.auth.decorators import login_required
import json
from django.http import JsonResponse
from .models import Post, Comment
from .forms import PostForm
# Create your views here.

@login_required(login_url="accounts:signin") #restricted to only logged in users 
def home(request):
    """ get and display all posts and comments made by users """
    posts = Post.objects.all() 
    comments = Comment.objects.all() 
    context = {'posts':posts, 'comments':comments}
    return render(request, 'home/home.html', context)

def post(request):
    author = get_object_or_404(User, id=request.user.id)
    if request.method != "POST":
        form = PostForm()
    else:
        photo = request.FILES['photo'] #get posted image
        caption = request.POST['caption'] #get the caption
        form = PostForm(request.POST, request.FILES) 
        if form.is_valid():
            Post.objects.create(author=author, caption=caption, photo=photo) #create new post with POST data
            return HttpResponseRedirect(reverse('accounts:profile', args=(author,)))
    context = {'form':form}
    return render(request, 'home/post.html', context)

def like_and_unlike_post(request):
    data = json.loads(request.body) #get JSON data sent from javascript 
    person = request.user.id #get current user ID
    postid = data['postID'] #get JSON data (postID) sent from javascript and save in a variable called postid 
    post = get_object_or_404(Post, id=postid) #get post object 
    user = get_object_or_404(User, id=person) #get user object
    if user in post.likes.all(): #if user object is in list of likes relationship remove user 
        unlike = post.likes.remove(user) 
    else:
        like = post.likes.add(user) #else add user to list of likes 
    data['likesNumber'] = post.number_of_likes() #get number of post likes in database and store in Json to send back to javascript
    return JsonResponse(data)

def comment(request):
    """ javascript handles request to comment on a post without page reload (illusion) """
    data = json.loads(request.body) #get JSON data sent from javascript
    postID = data['postID'] #get JSON data (post ID) sent from javascript
    userID = request.user.id 
    comment = data['comment'] #get JSON data (comment text) sent from javascript
    post = get_object_or_404(Post, id=postID)
    user = get_object_or_404(User, id=userID)
    Comment.objects.create(post=post, author=user, text=comment) #create comment in database
    return JsonResponse(data)

def comments(request, post_caption):
    post = get_object_or_404(Post, caption=post_caption)
    comments = post.comment_set.all()
    context = {'comments':comments}
    return render(request, 'home/comments.html', context)
