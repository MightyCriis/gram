from django.urls import path 
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home, name='home'),
    path('post/', views.post, name='post'),
    path('comment/', views.comment, name='comment'),
    #path('like/', views.like, name='like'),
    #path('unlike/', views.unlike, name='unlike'), 
    path('like_unlike/', views.like_and_unlike_post, name='likeandunlike'),
    path('<str:post_caption>/', views.comments, name='comments'),
]