from django.shortcuts import render, HttpResponse, HttpResponseRedirect, reverse, get_object_or_404
import json
from django.http import JsonResponse
from django.contrib.auth.models import User 
from django.contrib.auth import login, authenticate, logout
from .forms import SignUpForm, SignInForm, EditProfileForm, UserEditForm
from home.models import Post
from .models import Profile

# Create your views here.

def signup(request):
    if request.method != 'POST': #display empty form if request is GET
        form = SignUpForm()
    else:
        """ get the form filled in form to authenticated and login after signup """
        username = request.POST['username']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password1 = request.POST['password1']
        form = SignUpForm(data=request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(request, username=username, password=password1) 
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('home:home'))
            return HttpResponse('error, signup failed!')
    context = {'form':form}
    return render(request, 'registration/signup.html', context)

def signin(request):
    if request.method != 'POST':
        form = SignInForm()
    else:
        username = request.POST['username']
        password = request.POST['password']
        form = SignInForm(data=request.POST)
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('home:home'))
        return HttpResponse('error, signin failed!')
    context = {'form':form}
    return render(request, 'registration/login.html', context)

def profile(request, username):
    user = get_object_or_404(User, username=username)
    following = Profile.objects.filter(followers=user).count() #search for username in all profiles and count the number of occurence
    posts = user.post_set.all()
    context = {'user':user, 'following':following, 'posts':posts}
    return render(request, 'accounts/profile.html', context)

def edit(request):
    if request.method != 'POST':
        """ if GET request, show form with filled in data form the current user """
        user_form = UserEditForm(instance=request.user) 
        profile_form = EditProfileForm(instance=request.user.profile)
    else: 
        user_form = UserEditForm(request.POST, instance=request.user)
        profile_form = EditProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return HttpResponseRedirect(reverse('accounts:profile', args=(request.user.username,)))
        return HttpResponse('error, update failed!')
    context = {'user_form':user_form, 'profile_form':profile_form}
    return render(request, 'accounts/editprofile.html', context)

def follow_and_unfollow(request):
    data = json.loads(request.body)
    person = request.user.id
    profileID = data['profileID']
    user = get_object_or_404(User, id=person)
    profile = get_object_or_404(Profile, id=profileID)
    if user in profile.followers.all():
        unfollow = profile.followers.remove(user)
    else:
        follow = profile.followers.add(user)
    data['likeCount'] = profile.number_of_followers()
    return JsonResponse(data)

def follow_profile(request): 
    """ javascript handles request to follow profile without page roload """
    data = json.loads(request.body) #load JSON data sent from javascript and store in data
    person = request.user.id
    profileID = data['profile'] #get requested profile ID from JSON data 
    user = get_object_or_404(User, id=person) #get current user 
    profile = get_object_or_404(Profile, id=profileID) #get requested profile user ID 
    follow = profile.followers.add(user) #add current user to list of requested profileID 
    data['likeCount'] = profile.number_of_followers() #get the number of requested profileID followers as JSON to output in template through javascript
    return JsonResponse(data)

def unfollow_profile(request):
    """ javascript handles request to unfollow profile without page roload """
    data = json.loads(request.body) #load JSON data sent from javascript and store in data
    person = request.user.id
    profileID = data['profile'] #get requested profile ID from JSON data 
    user = get_object_or_404(User, id=person) #get current user 
    profile = get_object_or_404(Profile, id=profileID) #get requested profile user ID 
    follow = profile.followers.remove(user) #remove current user to list of requested profileID 
    data['likeCount'] = profile.number_of_followers() #get the number of requested profileID followers as JSON to output in template through javascript
    return JsonResponse(data)

def delete_post(request):
    data = json.loads(request.body)
    postid = data['postID']
    user = get_object_or_404(User, id=request.user.id)
    post = get_object_or_404(Post, id=postid)
    post.delete()
    return JsonResponse(data)

def signout(request):
    logout(request)
    return HttpResponseRedirect(reverse('accounts:signin'))