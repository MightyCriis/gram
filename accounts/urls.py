from django.urls import path 
from . import views 

app_name = 'accounts'

urlpatterns = [
    path('signup/', views.signup, name="signup"),
    path('signin/', views.signin, name='signin'),
    path('signout/', views.signout, name='signout'),
    path('edit/', views.edit, name='edit'),
    path('follow_and_unfollow/', views.follow_and_unfollow, name='follow_and_unfollow'),
    path('follow/', views.follow_profile, name='follow'),
    path('unfollow/', views.unfollow_profile, name='unfollow'),
    path('delete_post/', views.delete_post, name='delete_post'),
    path('<str:username>/', views.profile, name='profile')
]