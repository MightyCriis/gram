//using jquery for ajax 
$(document).ready(function(){
    $('.follow').click(function(){
        var profileID = this.dataset.profile;
        console.log(profileID);
        $.ajax({
            url: '/user/follow_and_unfollow/',
            method: 'POST',
            data: JSON.stringify({'profileID':profileID}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            success: function(data){
                var followbtn = $(".follow[data-profile="+ profileID +"]");
                var btntext = followbtn.text();
                var followersCount = data.likeCount;
                if(btntext === 'Follow'){
                    followbtn.css({'backgroundColor':'#f7f7f7', 'color':'black'}).text('Following');
                }
                else{
                    followbtn.css({'backgroundColor':'#007bff', 'color': 'white'}).text('Follow');
                }

                if (followersCount == 1) {
                    $(".followers_number[data-profile="+ profileID + "]" ).html(followersCount + ' follower');
                }
                else {
                    $(".followers_number[data-profile="+ profileID + "]" ).html(followersCount + ' followers');
                }
            }
        })
    })
})


$(document).ready(function(){
    $('.following').click(function(){
        var profileID = this.dataset.profile;
        console.log(profileID);
        $.ajax({
            url: '/user/follow_and_unfollow/',
            method: 'POST',
            data: JSON.stringify({'profileID':profileID}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            success: function(data){
                var followingbtn = $(".following[data-profile="+ profileID +"]");
                var btntext = followingbtn.text();
                var followersCount = data.likeCount;
                if(btntext === 'Following'){
                    followingbtn.css({'backgroundColor':'#007bff', 'color':'white'}).text('Follow');
                }
                else{
                    followingbtn.css({'backgroundColor':'#f7f7f7', 'color': 'black'}).text('Following');
                }
                
                if (followersCount == 1) {
                    $(".followers_number[data-profile="+ profileID + "]" ).html(followersCount + ' follower');
                }
                else {
                    $(".followers_number[data-profile="+ profileID + "]" ).html(followersCount + ' followers');
                }
            }
        })
    })
})

$(document).ready(function(){
    $('.deletepost').click(function(){
        var postid = this.dataset.postid;
        console.log(postid)
        $.ajax({
            url: '/user/delete_post/',
            type: 'POST',
            data: JSON.stringify({'postID':postid}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            success: function(data){
                $(".deleteimage[data-postID="+ data.postID +"]").hide();
            }
        })
    })
})