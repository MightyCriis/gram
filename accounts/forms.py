from django import forms 
from django.contrib.auth.forms import UserCreationForm
from .models import Profile
from django.contrib.auth.models import User 


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, required=True, help_text='Required')
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    password1 = forms.CharField(label='Enter password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name' ,'password1', 'password2')

class SignInForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)

class UserEditForm(forms.ModelForm):
    class Meta:
        model = User 
        fields = ('username',)

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile 
        fields = ('profile_photo', 'bio')